#!/usr/bin/env node
require('@babel/register')({
	plugins: [[
		'module-resolver',
		{ alias: { '~': './' } }
	]],
	presets: [[
		'@babel/preset-env',
		{ targets: { node: true } }
	]]
});

require('./main');

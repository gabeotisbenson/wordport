/* global URL */
// Required modules
import app from '~/app';
import cleanUpDocument from '~/parser/cleanup';
import consola from 'consola';
import Counter from '~/counter';
import filetypes from '~/filetypes';
import fs from 'fs';
import path from 'path';
import preRenderCleanup from '~/pre-render';
import request from 'superagent';
import { getXml } from '~/render';
import { JSDOM } from 'jsdom';
import {
	getFilename,
	getSanitizedHref,
	matchesAny,
	mediaTree,
	pageTree,
	urlTree
} from '~/utilities';

// Grab the url and make it proper if necessary
let siteUrl = app.args[0];
if (siteUrl.indexOf('http') !== 0) siteUrl = `http://${siteUrl}`;
if (!matchesAny(app.config.htmlTypes, siteUrl)) siteUrl += '/';
siteUrl = new URL(siteUrl);
app.config.siteRoot = app.config.mode === 'subsite' ? siteUrl.href.replace(/\/[^/]+$/, '/') : siteUrl.hostname;

// A counter to keep track of recursions.  When the counter hits zero, it runs
// the function passed in as an argument.  Async + recursion = the need for this
const counter = new Counter(() => {
	if (app.config.logLevel > 1) {
		consola.info(`${pageTree.size} pages parsed`);
		consola.info(`${mediaTree.size} media parsed`);
	}

	if (pageTree.size < 1) {
		consola.error('No pages were found, there is nothing to import.');
		process.exit(1);
	} else {
		// Run the final cleanup
		consola.info('Site parsed, cleaning up pages...');
		preRenderCleanup(pageTree, mediaTree);

		// Output path is user's current working directory, plus the output file they
		// specified or the fallback if they didn't specify.
		const outputPath = path.resolve(process.cwd(), app.output || 'wordport.xml');

		// Write the file!
		fs.writeFile(outputPath, getXml(pageTree, mediaTree), 'utf8', err => {
			if (err) {
				consola.error(err);
			} else {
				consola.success(`Done! File written to ${outputPath}`);
			}
		});
	};
});

// Another counter for ids, guarantees a unique id for every page/media
let idCounter = 101;

// Basically our main method, takes in a url and starts the whole process.
const generateXml = url => {
	// Update user on what we're doing
	consola.info(`Attempting to generate XML for site: ${url}`);

	// Function to determine if a page is needed or not
	const needPage = anchor => {
		// Is the url on the same domain as the requested site?
		if (anchor.hostname !== siteUrl.hostname) return false;
		// If in subsite mode, is the link to a page within the subsite?
		if (app.config.mode === 'subsite' && anchor.href.indexOf(siteUrl.href.replace(/[^/]+$/, '')) === -1) return false;
		// Is it under a path we should ignore?
		if ((() => {
			let shouldIgnore = false;
			app.config.pathsToIgnore.forEach(path => {
				if (anchor.pathname.indexOf(path) === 0) shouldIgnore = true;
			});
			return shouldIgnore;
		})()) return false;
		// Have we already fetched this url?
		if (urlTree.find(getSanitizedHref(anchor))) return false;
		if (/index\.[^.]+$/.test(getSanitizedHref(anchor)) && urlTree.find(getSanitizedHref(anchor).replace(/index\.[^.]+$/, ''))) return false;
		// Is this url likely to be for an html page?
		let isHtml = false;
		if (Array.isArray(app.config.htmlTypes)) {
			app.config.htmlTypes.forEach(type => {
				if (anchor.pathname.indexOf(type) !== -1) isHtml = true;
			});
		}
		// If it isn't for a literal html file, but it's actually not for *any* file
		// then it's probably html.
		if (!isHtml && !/\/.*\..*$/.test(anchor.pathname)) isHtml = true;
		return isHtml;
	};

	// Function to determine if media is needed or not
	const needMedia = url => {
		// Should we be including media in the first place?
		if (!app.config.includeMedia) return false;
		// Is the url on the same domain as the requested site?
		if (url.hostname !== siteUrl.hostname) return false;
		// Have we already added this url?
		if (urlTree.find(url.href)) return false;
		// Is it a filetype we care about?
		if (filetypes.indexOf(url.pathname.toLowerCase().replace(/.*\.([^.]+)$/, '$1')) === -1) return false;
		// Add it!
		urlTree.insert(url.href);
		return true;
	};

	// Recursive function that handles the fetching and parsing of a page.
	const fetchUrl = async (url) => {
		url = url.replace(/\/$/, '');
		// Handy little error handler
		const requestErrorCatcher = (e, url) => {
			// Log the error
			if (app.config.logLevel > 1) consola.warn(`HTTP ${e.status} - ${url}`);
			// And decrement the counter
			counter.decrement();
		};

		// For every url we fetch, we need to increment the counter
		counter.increment();
		if (!urlTree.find(url)) {
			urlTree.insert(url);
			if (app.config.logLevel > 1) consola.info(`Fetching ${url}`);
			const res = await request.get(url).catch(e => { requestErrorCatcher(e, url); });
			if (app.config.logLevel > 2) consola.log(`Got ${url}`);
			// If the request errored out and we don't have a response, exit this
			// function
			if (!res) return;
			// Determine if it's a link to an html page
			const isHtml = res.headers['content-type'].indexOf('text/html') !== -1;
			if (app.config.logLevel > 2) consola.log(`${url} is ${isHtml ? 'html' : 'NOT html'}`);
			// If it is...
			if (isHtml) {
				// Set up a fake dom for the page
				const { document } = (new JSDOM(res.text, { url })).window;

				// Our 'done parsing page' function
				const finalizePage = (page, document) => {
					// Grab the title
					const title = [document.querySelector(app.config.titleSelector), document.querySelector('title'), 'Unknown'].reduce((title, item) => {
						if (typeof title === 'string') return title;
						if (title && title.textContent) return title.textContent.trim();
						return item;
					});
					// The Body
					const body = document.querySelector('body');
					// And the content
					const content = app.config.contentSelector === 'body' ? body : body.querySelector(app.config.contentSelector);
					if (!content) {
						if (app.config.logLevel > 0) consola.warn(`Could not find content for page found at ${url}`);
						counter.decrement();
						return;
					}
					// Clean up the document
					cleanUpDocument(document);
					// Complete our page object with that data
					page.title = title;
					page.content = content.innerHTML;
					// And push it into our page tree
					pageTree.insert(page);
					counter.decrement();
				};

				// And create an object representing the page that will eventually be
				// inserted into the page tree
				const page = { id: idCounter++, parentId: 0, url };

				// Then, scan the page for links/images/forms
				const linksAndImages = document.querySelectorAll('a[href], img[src], form[action]');
				// If there are no links or images, we're done
				if (linksAndImages.length < 1) {
					finalizePage(page, document);
				} else {
					linksAndImages.forEach((item, i) => {
						if (item.tagName === 'A') {
							// We got a link here, boys!
							if (needPage(item)) fetchUrl(getSanitizedHref(item), page.id);
							try {
								const itemUrl = new URL(item.href);
								if (needMedia(itemUrl)) addMedia(item.href, item.innerText || item.textContent, page.id);
							} catch (e) {
								if (app.config.logLevel > 2) consola.error(`Invalid URL found: ${item.href}`);
							}
						} else if (item.tagName === 'IMG') {
							// Aww man, an image again?!
							try {
								const itemUrl = new URL(item.src);
								if (needMedia(itemUrl)) addMedia(item.src, (item.getAttribute('alt') || getFilename(item.src)).trim(), page.id);
							} catch (e) {
								if (app.config.logLevel > 2) consola.error(`Invalid URL found: ${item.href}`);
							}
						} else if (item.tagName === 'FORM') {
							// Handle form actions
							try {
								const formUrl = new URL(item.action);
								if (needPage(formUrl)) fetchUrl(getSanitizedHref(formUrl), page.id);
								if (needMedia(formUrl)) addMedia(formUrl.href, item.innerText || item.textContent, page.id);
							} catch (e) {
								if (app.config.logLevel > 2) consola.error(`Invalid URL found: ${item.href}`);
							}
						}
						if (i === linksAndImages.length - 1) finalizePage(page, document);
					});
				}
			} else {
				// Url is not HTML
				counter.decrement();
			}
		} else {
			// Url is already in tree
			counter.decrement();
		}
	};
	fetchUrl(url.href);
};

const addMedia = (url, title, parentId) => {
	// Extract some data
	mediaTree.insert({
		id: idCounter++,
		title,
		parentId,
		url
	});
};

generateXml(siteUrl);

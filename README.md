# wordport
`wordport` is a friendly little command-line application written in JavaScript
that aims to make migrating a static website into WordPress quick and painless.

It should be noted that this is **not** a perfect process.  It is likely that
you will still want to do a bit of housecleaning after migrating your site into
WordPress using this tool.  However, wordport does attempt to do much of the
work for you.

Features include:
- **Automatic Content Processing**
  - No more copying and pasting your content into WordPress manually.  wordport
	generates a nice little XML file that you may import into WordPress to
	automatically create all of your pages for you.
- **Automatic Media/Document Uploading**
	- All media and documents found on your site will be processed and uploaded
	into your new WordPress site.
- **Updating of Links and Images**
	- All links and images found in the pages of your site will be updated to
	point to their new locations after your site has been migrated into WordPress.
- **Removal of Deprecated Markup**
	- wordport will check your site for deprecated HTML tags and attributes, and
	will automatically remove them before your site is migrated.

## Usage
You may either install globally via `npm i -g wordport` or simply run using npx:
`npx wordport`.

After installing, simply point wordport at the site for which you would like to
generate a WordPress export XML file:
```shell
$ wordport http://mysite.com
```

Once wordport has finished, a "wordport.xml" file can be found in the directory
where you ran wordport.  This file may then be uploaded into your WordPress site
using WordPress' import feature found in the admin interface at "Tools > Import >
WordPress > Run Importer".

### General Help
```
$ wordport --help

Usage: index [options] <url>

Options:
  -V, --version        output the version number
  -c, --config <file>  Path to configuration file.  See https://gitlab.com/gabeotisbenson/wordport#configuration-file for examples.
  -o, --output <file>  File to save XML content in, defaults to "wordport.xml"
  -h, --help           output usage information
```

### Configuration File
Optionally (though it's **strongly** recommended), you may pass a .json or .js
configuration file into wordport to further refine the processing of your
existing site.  You do not need to specify a value for every option, rather, you
need only specify values for the fields you would like to overwrite.

For example, if you would simply like to exclude media during migration and
would like to keep all inline styling present, your configuration file would
look like this:
```json
{
	"includeMedia": false,
	"stripInlineStyling": false
}
```

See below for all available options, their effects, and their
defaults:
```js
{
	// A username to use as the author of all processed pages and media
	"author": "wordport",

	// A CSS selector for the element containing the content of each page.  For
	// instance, if a page's content is found with a <div> tag with an id of
	// "main", you would put "div#main" here.
	"contentSelector": "body",

	// An array of filetypes to consider when looking for html pages
	"htmlTypes": [".htm", ".html"],

	// A boolean that determines whether or not media (images, documents, etc.)
	// will be processed as part of the migration.
	"includeMedia": true,

	// A number representing the level of verbosity for status and error logs
	"logLevel": 1,

	// Additional filetypes to consider when parsing your site for media.  By
	// default all filetypes accepted by WordPress are considered.  You may find
	// the list of those types at
	// https://en.support.wordpress.com/accepted-filetypes/
	"extraMediaTypes": [],

	// The parsing mode.  The default is 'subsite', which will only consider pages
	// found at the same level or underneath the URL at which you point wordport.
	// e.g. if 'http://mysite.com/zebras' is the URL used, only pages found
	// beneath the 'zebras' directory will be processed.
	//
	// Any mode other than "subsite" will have wordport consider all pages found
	// at the same domain to be a part of the site you're trying to migrate.
	"mode": "subsite",

	// An array of urls which will not be considered when processing your site.
	// e.g. 'http://mysite.com/zebras' will cause any pages at or under the
	// 'zebras' directory to be ignored.
	"pathsToIgnore": [],

	// A boolean stating whether or not to remove deprecated tags when processing
	// your site.
	"removeDeprecatedTags": true,

	// The path to your WordPress installation.  If WordPress is running at a
	// a level other than the root of your new site, you need to append it here.
	// e.g. '/wordpress' for a WordPress site found at http://mysite.com/wordpress
	"wordpressPath": "/"

	// An array of CSS selectors to be removed when parsing each page's content.
	// For instance, if you have breadcrumbs at the top of every page that you'd
	// like to not keep when migrating your site, you might use "p.breadcrumbs" to
	// remove any <p> tags with a class of "breadcrumbs"
	"selectorsToRemove": [],

	// A boolean stating whether or not custom classes should be removed from the
	// HTML of your pages during migration.
	"stripClasses": true,

	// A boolean stating whether or not deprecated HTML attributes should be
	// removed from the HTML of your pages during migration.
	"stripDeprecatedAttributes": true,

	// A boolean stating whether or not inline styling should be removed from the
	// HTML of your pages during migration.
	"stripInlineStyling": true,

	// A CSS selector for determining the title of each page of your site.  If an
	// element matching the selector is not found, the title tag of the page is
	// used instead.
	"titleSelector": "title"
}
```

## Bugs
If you find a bug, please please please
[report it](https://gitlab.com/gabeotisbenson/wordport/issues/new)!

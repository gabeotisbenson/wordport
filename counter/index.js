class Counter {
	constructor (callback = () => {}) {
		this.internalCount = 0;
		this.callback = callback;
	}

	get count () { return this.internalCount; }

	increment (val = 1) {
		this.internalCount += val;
	}

	decrement (val = 1) {
		if (this.internalCount > 0) this.internalCount -= val;
		if (this.internalCount === 0) return this.callback();
	}
}

export default Counter;

// Required modules
import app from '~/app';
import dateFormat from 'dateformat';
import { RBTree } from 'bintrees';
import { JSDOM } from 'jsdom';
import {
	getFilename,
	getFilenameWithExtension,
	getPathWithoutFilename,
	getSanitizedHref
} from '~/utilities';

// Little constant for uploads folder
const now = new Date();
const uploadsPath = `${app.config.wordpressPath}wp-content/uploads/${dateFormat(now, 'yyyy')}/${dateFormat(now, 'mm')}/`;

// Function to determine if URL is at the root
const isRoot = url => {
	// First, get rid of any extraneous info (http://)
	url = url.replace(/^https?:\/\//, '').replace(/\/$/, '');
	// Is the url literally for the site root?
	if (url === app.config.siteRoot) return true;
	// Is the url plus a slash for the site root?
	if (url + '/' === app.config.siteRoot) return true;
	// Is the url plus index plus one of our html types for the site root?
	for (let i = 0; i < app.config.htmlTypes.length; ++i) {
		let type = app.config.htmlTypes[i];
		if (url === `${app.config.siteRoot}index${type}`) return true;
	}
	// If we move up one level are we at the site root?
	let upOneLevel = url.replace(/\/[^/]+\/?$/, '/');
	if (upOneLevel === app.config.siteRoot) return true;
	return false;
};

const cleanUpMediaTitles = mediaTree => {
	const titleTree = new RBTree((a, b) => {
		if (a === b) return 0;
		if (a > b) return 1;
		return -1;
	});
	mediaTree.each(m => {
		if (!titleTree.find(m.title)) {
			// Title is unique, so push this into the title tree and move on
			titleTree.insert(m.title);
		} else {
			// Title is not unique
			// So, let's come up with a new title
			m.title = `${m.title} - ${getFilename(m.url)}`;
			let count = 2;
			if (titleTree.find(m.title)) m.title += `(${count++})`;
			while (titleTree.find(m.title)) {
				m.title = m.title.replace(/\([0-9]+\)$/, `(${count++})`);
				if (!titleTree.find(m.title)) {
					titleTree.insert(m.title);
					break;
				}
			}
		}
	});
};

const getParentId = (url, pageTree) => {
	// If the url is at the root of the site, there is no parent
	if (isRoot(url)) return 0;
	// Get the root path for this url (moving up a level automatically if this url
	// is an index page)
	let rootUrl = getPathWithoutFilename(/index\.[^/]+$/.test(url) ? url.replace(/\/[^/]+$/, '') : url);
	// Assume we haven't found a parent, and set the id to 0 (root)
	let foundParent = false;
	let parentId = 0;
	let parent = pageTree.find({ url: rootUrl });
	if (parent) {
		parentId = parent.id;
		foundParent = true;
	} else {
		// Loop through the available types for an html page
		app.config.htmlTypes.forEach((type, i) => {
			// And try to find a page with that path
			let parent = pageTree.find({ url: `${rootUrl}index${type}` });
			// If a parent is found
			if (parent) {
				// Flip our bit and set the id
				foundParent = true;
				parentId = parent.id;
			}
		});
	}
	// If we found the parent, return the id!
	if (foundParent) {
		return parentId;
	} else {
		// Otherwise, let's recurse!
		return getParentId(rootUrl.replace(/\/$/, ''), pageTree);
	}
};

const fixLinksAndImages = (page, pageTree, mediaTree) => {
	const { document } = (new JSDOM(page.content, { url: page.url })).window;
	document.querySelectorAll('a[href]').forEach(anchor => {
		// If this is a link to a page in our tree...
		if (anchor.href.indexOf(app.config.siteRoot) !== -1) {
			let destination = pageTree.find({ url: getSanitizedHref(anchor.href) }) || pageTree.find({ url: getSanitizedHref(anchor.href).replace(/index\.[^./]+$/, '') });
			if (destination) {
				anchor.setAttribute('href', `${app.config.wordpressPath}?page_id=${destination.id}${anchor.hash}`);
			} else {
				destination = mediaTree.find({ url: getSanitizedHref(anchor.href) });
				if (destination) anchor.setAttribute('href', `${uploadsPath}${getFilenameWithExtension(destination.url)}`);
			}
		}
	});
	document.querySelectorAll('img').forEach(image => {
		if (image.src.indexOf(app.config.siteRoot) !== -1) {
			const media = mediaTree.find({ url: image.src });
			if (media) image.setAttribute('src', `${uploadsPath}${getFilenameWithExtension(media.url)}`);
		}
	});
	page.content = document.documentElement.innerHTML;
};

const preRenderCleanup = (pageTree, mediaTree) => {
	cleanUpMediaTitles(mediaTree);
	pageTree.each(page => {
		page.parentId = getParentId(page.url, pageTree);
		fixLinksAndImages(page, pageTree, mediaTree);
	});
};

export default preRenderCleanup;

/* global URL */
// Required modules
import { RBTree } from 'bintrees';

// Function to get filename (with+without extension) from url
export const getFilename = url => url.replace(/.*\/([^/]+)$/, '$1').replace(/\.[^.]+/, '');
export const getFilenameWithExtension = url => url.replace(/.*\/([^/]+)$/, '$1');

// Function to get url minus filename
export const getPathWithoutFilename = url => /\/$/.test(url) ? url.replace(/\/[^/]+\/$/, '/') : url.replace(/\/[^/]+$/, '/');

// Function to clean up a url
export const getSanitizedHref = (url) => {
	if (typeof url === 'string') url = new URL(url);
	let sanitizedUrl = `${url.origin}${url.pathname}`;
	// Need to check if the url is for a directory, and if so, append a trailing
	// slash (if it doesn't already have one)
	if (!/\.[^./]+$/.test(sanitizedUrl) && !/\/$/.test(sanitizedUrl)) sanitizedUrl += '/';
	return sanitizedUrl;
};

// Function to check if a string contains any strings in an array
export const matchesAny = (arr, str) => {
	if (!arr) return false;
	let matches = false;
	arr.forEach(i => {
		if (str.indexOf(i) !== -1) matches = true;
	});
	return matches;
};

// Tree for holding files
export const mediaTree = new RBTree((a, b) => {
	if (a.url === b.url) return 0;
	if (a.url > b.url) return 1;
	return -1;
});

// Tree for pages
export const pageTree = new RBTree((a, b) => {
	if (a.url === b.url) return 0;
	if (a.url > b.url) return 1;
	return -1;
});

// String to slug
export const stringToSlug = str => str.toLowerCase().replace(/[^a-zA-Z0-9 ]/g, '').replace(/\s+/g, '-');

// Tree to track urls requested/added
export const urlTree = new RBTree((a, b) => {
	if (a === b) return 0;
	if (a > b) return 1;
	return -1;
});

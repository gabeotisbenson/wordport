// Required modules
import app from 'commander';
import consola from 'consola';
import defaultConfig from '~/config.example';
import fs from 'fs';
import pkg from '~/package';

// Create the application
app
	.version(pkg.version)
	.usage('[options] <url>')
	.option('-c, --config <file>', 'Path to configuration file.  See https://gitlab.com/gabeotisbenson/wordport#configuration-file for examples.', f => Object.assign(defaultConfig, JSON.parse(fs.readFileSync(f))))
	.option('-o, --output <file>', 'File to save XML content in, defaults to "wordport.xml"')
	.parse(process.argv);

// Default config if not specified
if (!app.config) app.config = defaultConfig;

const haltAndCatchFire = (message = 'An error was encountered.') => {
	consola.error(message);
	app.outputHelp();
	process.exit(-1);
};

// Validate arguments
if (app.args.length < 1) haltAndCatchFire('You must specify a site to import!');
if (app.args.length > 1) haltAndCatchFire('You may only import one site at a time!');

export default app;

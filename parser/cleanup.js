import app from '~/app';

const cleanUpWhitespace = content => {
	if (!content) return;
	content.innerHTML = content.innerHTML
		.replace(/[\n\t]+/g, '')
		.replace(/ +/g, ' ');
};

const removeClasses = el => { if (el.hasAttribute('class')) el.removeAttribute('class'); };

const removeComments = content => {
	if (!content) return;
	content.innerHTML = content.innerHTML.replace(/<!-{2,3}[\s\S]*?-{2,3}>/g, '');
};

const removeStyles = el => {
	[
		'style',
		'color',
		'height',
		'width'
	].forEach(attr => { if (el.hasAttribute(attr)) el.removeAttribute(attr); });
};

const removeIfEmpty = el => {
	if (el.innerHTML === '') {
		const parent = el.parentNode;
		parent.removeChild(el);
		removeIfEmpty(parent);
	}
};

const removeDeprecatedAttributes = el => {
	const headingDeprecations = ['align'];
	const deprecations = {
		a: ['rev', 'charset', 'shape', 'coords'],
		area: ['nohref'],
		caption: ['align'],
		div: ['align'],
		h1: headingDeprecations,
		h2: headingDeprecations,
		h3: headingDeprecations,
		h4: headingDeprecations,
		h5: headingDeprecations,
		h6: headingDeprecations,
		hr: ['align', 'noshade', 'size'],
		iframe: ['longdesc'],
		img: ['longdesc', 'name'],
		input: ['align'],
		legend: ['align'],
		link: ['rev', 'charset'],
		p: ['align'],
		table: ['align', 'bgcolor', 'border', 'frame', 'rules', 'width'],
		tbody: ['align', 'bgcolor', 'char', 'charoff', 'valign'],
		td: ['axis', 'abbr', 'scope', 'align', 'bgcolor', 'char', 'charoff', 'nowrap', 'valign', 'width'],
		tfoot: ['align', 'char', 'charoff', 'valign'],
		th: ['align', 'bgcolor', 'char', 'charoff', 'nowrap', 'valign', 'width'],
		thead: ['align', 'char', 'charoff', 'valign'],
		tr: ['align', 'bgcolor', 'char', 'charoff', 'valign']
	};
	if (deprecations[el.tagName.toLowerCase()]) {
		deprecations[el.tagName.toLowerCase()].forEach(attr => {
			if (el.hasAttribute(attr)) el.removeAttribute(attr);
		});
	}
};

const removeDeprecatedTags = el => {
	if (!el) return;
	const deprecatedTags = ['font'];
	deprecatedTags.forEach(tag => {
		if (el.tagName.toLowerCase() === tag) el.outerHTML = el.innerHTML;
	});
};

const removeSelectors = document => {
	app.config.selectorsToRemove.forEach(selector => {
		document.querySelectorAll(selector).forEach(el => {
			el.parentNode.removeChild(el);
		});
	});
};

const cleanUpDocument = document => {
	// Remove any tags specified in the config
	removeSelectors(document);

	// Get rid of HTML comments
	removeComments(document.querySelector(app.config.contentSelector));

	// Clean up whitespace
	cleanUpWhitespace(document.querySelector(app.config.contentSelector));

	document.querySelectorAll('*').forEach(el => {
		// Remove custom classes
		if (app.config.stripClasses) removeClasses(el);

		// Remove inline styling
		if (app.config.stripInlineStyling) removeStyles(el);

		// Remove deprecated attributes
		if (app.config.stripDeprecatedAttributes) removeDeprecatedAttributes(el);

		// Remove deprecated tags
		if (app.config.removeDeprecatedTags) removeDeprecatedTags(el);
	});
};

export default cleanUpDocument;

// List of accepted filetypes, gotten from Wordpress functions file
// and pruned to remove any web files (html, js, css, etc.)
// File is: 'wp-includes/functions.php'

const audioFormats = [
	'mp3',
	'm4a',
	'm4b',
	'aac',
	'ra',
	'ram',
	'wav',
	'ogg',
	'oga',
	'flac',
	'mid',
	'midi',
	'wma',
	'wax',
	'mka'
];

const imageFormats = [
	'jpg',
	'jpeg',
	'jpe',
	'gif',
	'png',
	'bmp',
	'tiff',
	'tif',
	'ico'
];

const iWorkFormats = [
	'key',
	'numbers',
	'pages'
];

const miscApplicationFormats = [
	'rtf',
	'pdf',
	'tar',
	'zip',
	'gz',
	'gzip',
	'rar',
	'7z',
	'psd',
	'xcf'
];

const msOfficeFormats = [
	'doc',
	'pot',
	'pps',
	'ppt',
	'wri',
	'xla',
	'xls',
	'xlt',
	'xlw',
	'mdb',
	'mpp',
	'docx',
	'docm',
	'dotx',
	'dotm',
	'xlsx',
	'xlsm',
	'xlsb',
	'xltx',
	'xltm',
	'xlam',
	'pptx',
	'pptm',
	'ppsx',
	'ppsm',
	'potx',
	'potm',
	'ppam',
	'sldx',
	'sldm',
	'onetoc',
	'onetoc2',
	'onetmp',
	'onepkg',
	'oxps',
	'xps'
];

const openOfficeFormats = [
	'odt',
	'odp',
	'ods',
	'odg',
	'odc',
	'odb',
	'odf'
];

const textFormats = [
	'txt',
	'srt',
	'asc',
	'csv',
	'tsv',
	'ics',
	'rtx',
	'vtt',
	'dfxp'
];

const videoFormats = [
	'asf',
	'asx',
	'wmv',
	'wmx',
	'wm',
	'avi',
	'divx',
	'flv',
	'mov',
	'qt',
	'mpeg',
	'mpg',
	'mpe',
	'mp4',
	'm4v',
	'ogv',
	'webm',
	'mkv',
	'3gp',
	'3gpp',
	'3g2',
	'3gp2'
];

const wordPerfectFormats = [
	'wp',
	'wpd'
];

export default [
	...audioFormats,
	...imageFormats,
	...iWorkFormats,
	...miscApplicationFormats,
	...msOfficeFormats,
	...openOfficeFormats,
	...textFormats,
	...videoFormats,
	...wordPerfectFormats
];
